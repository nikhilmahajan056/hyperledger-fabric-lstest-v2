
# Delete existing artifacts
# rm genesis.block userchannel.tx
# rm -rf ../../channel-artifacts/*

#Generate Crypto artifactes for organizations
# cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/



# System channel
SYS_CHANNEL="sys-channel"

# channel name defaults to "userchannel"
CHANNEL_NAME="cjchannel"
ORG1_CHANNEL_NAME="org1userchannel"
ORG2_CHANNEL_NAME="org2userchannel"

echo $ORG1_CHANNEL_NAME
echo $ORG2_CHANNEL_NAME

# Generate System Genesis block
configtxgen -profile OrdererGenesis -configPath . -channelID $SYS_CHANNEL  -outputBlock ./genesis.block


# Create separate channel for each organization

# Generate channel configuration block org1
# configtxgen -profile BasicChannel -configPath . -outputCreateChannelTx ./$CHANNEL_NAME.tx -channelID $CHANNEL_NAME
configtxgen -profile Org1Channel -configPath . -outputCreateChannelTx ./$ORG1_CHANNEL_NAME.tx -channelID $ORG1_CHANNEL_NAME

echo "#######    Generating anchor peer update for Org1MSP  ##########"
# configtxgen -profile BasicChannel -configPath . -outputAnchorPeersUpdate ./Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP
configtxgen -profile Org1Channel -configPath . -outputAnchorPeersUpdate ./Org1MSPanchors.tx -channelID $ORG1_CHANNEL_NAME -asOrg Org1MSP

# Generate channel configuration block org2
# configtxgen -profile BasicChannel -configPath . -outputCreateChannelTx ./$CHANNEL_NAME.tx -channelID $CHANNEL_NAME
configtxgen -profile Org2Channel -configPath . -outputCreateChannelTx ./$ORG2_CHANNEL_NAME.tx -channelID $ORG2_CHANNEL_NAME


echo "#######    Generating anchor peer update for Org2MSP  ##########"
# configtxgen -profile BasicChannel -configPath . -outputAnchorPeersUpdate ./Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP
configtxgen -profile Org2Channel -configPath . -outputAnchorPeersUpdate ./Org2MSPanchors.tx -channelID $ORG2_CHANNEL_NAME -asOrg Org2MSP


# Create common channel between two organizations for data sharing

# Generate channel configuration block org1
# configtxgen -profile BasicChannel -configPath . -outputCreateChannelTx ./$CHANNEL_NAME.tx -channelID $CHANNEL_NAME

# echo "#######    Generating anchor peer update for Org1MSP  ##########"
# configtxgen -profile BasicChannel -configPath . -outputAnchorPeersUpdate ./Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP


# echo "#######    Generating anchor peer update for Org2MSP  ##########"
# configtxgen -profile BasicChannel -configPath . -outputAnchorPeersUpdate ./Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP
