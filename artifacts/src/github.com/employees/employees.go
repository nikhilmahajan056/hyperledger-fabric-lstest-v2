package main

import (
	"fmt"
	"strconv"
	"encoding/json"
	"strings"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type Employee struct {
	EmployeeID        int 	 `json:"employeeID"`
	FirstName         string `json:"firstName"`
	LastName          string `json:"lastName"`
	ContactNumber     string `json:"contactNumber"`
	EmailID           string `json:"emailID"`
	Address           string `json:"address"`
	YearsOfExperience int    `json:"yearsOfExperience"`
	Technologies      string `json:"technologies"`
	IsAvailable       bool   `json:"isAvailable"`
}

type Salary struct {
	EmployeeID    int    `json:"employeeID"`
	SalaryDetails string `json:"salaryDetails"`
	DateOfJoining string `json:"dateOfJoining"`
}

type Project struct {
	EmployeeID       int    `json:"employeeID"`
	ProjectDetails   string `json:"projectDetails"`
	ReportingManager string `json:"reportingManager`
}

type EmployeeChaincode struct {
	contractapi.Contract
}

var employeeId int = 0 

func (ec *EmployeeChaincode) InitLedger(ctx contractapi.TransactionContextInterface) error {
	return nil
}

func (ec *EmployeeChaincode) InsertEmployee(ctx contractapi.TransactionContextInterface, firstName string, lastName string, contactNumber string, emailID string, address string, yearsOfExperience int, technologies string, isAvailable bool) error {
	employeeId += 1;
	employee := Employee{
		EmployeeID:        employeeId,
		FirstName:         firstName,
		LastName:          lastName,
		ContactNumber:     contactNumber,
		EmailID:           emailID,
		Address:           address,
		YearsOfExperience: yearsOfExperience,
		Technologies:      technologies,
		IsAvailable:       isAvailable,
	}

	employeeBytes, err := ctx.GetStub().GetState(strconv.Itoa(employeeId))
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
	}
	if employeeBytes != nil {
		return fmt.Errorf("employee with ID %d already exists", employeeId)
	}

	employeeJSON, err := json.Marshal(employee)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(strconv.Itoa(employeeId), employeeJSON)
	if err != nil {
		return fmt.Errorf("failed to write to world state: %v", err)
	}

	return nil
}

func (ec *EmployeeChaincode) UpdateEmployee(ctx contractapi.TransactionContextInterface, employeeId int, firstName string, lastName string, contactNumber string, emailID string, address string, yearsOfExperience int, technologies string, isAvailable bool) error {
	
	if len(strings.TrimSpace(strconv.Itoa(employeeId))) == 0 {

		return fmt.Errorf("Please pass the correct Employee ID. The Employee ID is Empty or contains only whitespaces.")
	}

	// Check whether given EmployeeID Already Exists or Not.
	exists, err := ec.EmployeeExists(ctx, employeeId)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("The Given EmployeeID: %s not Exists. Please try with correct existing EmployeeID.", employeeId)
	}

	employee := Employee{
		EmployeeID:        employeeId,
		FirstName:         firstName,
		LastName:          lastName,
		ContactNumber:     contactNumber,
		EmailID:           emailID,
		Address:           address,
		YearsOfExperience: yearsOfExperience,
		Technologies:      technologies,
		IsAvailable:       isAvailable,
	}

	employeeJSON, err := json.Marshal(employee)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(strconv.Itoa(employeeId), employeeJSON)
	if err != nil {
		return fmt.Errorf("failed to write to world state: %v", err)
	}

	return nil
}

// EmployeeExists returns true when employee with given ID exists in world state
func (ec *EmployeeChaincode) EmployeeExists(ctx contractapi.TransactionContextInterface, employeeId int) (bool, error) {

	if len(strings.TrimSpace(strconv.Itoa(employeeId))) == 0 {
		return false, fmt.Errorf("Please pass the correct Employee ID. The Employee ID is Empty or contains only whitespaces.")
	}

	employeeJSON, err := ctx.GetStub().GetState(strconv.Itoa(employeeId))
	if err != nil {
		return false, fmt.Errorf("Employee Exists Check Failed for the given EmployeeID. The Error is: %v", err)
	}

	return employeeJSON != nil, nil
}


func (ec *EmployeeChaincode) FetchEmployee(ctx contractapi.TransactionContextInterface, employeeID int) (*Employee, error) {
	employeeBytes, err := ctx.GetStub().GetState(strconv.Itoa(employeeID))
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if employeeBytes == nil {
		return nil, fmt.Errorf("employee with ID %d does not exist", employeeID)
	}

	var employee Employee
	err = json.Unmarshal(employeeBytes, &employee)
	if err != nil {
		return nil, err
	}

	return &employee, nil
}

func (ec *EmployeeChaincode) InsertPrivateSalaryData(ctx contractapi.TransactionContextInterface, employeeID int, salaryDetails string, dateOfJoining string) error {
	salary := Salary{
		EmployeeID:    employeeID,
		SalaryDetails: salaryDetails,
		DateOfJoining: dateOfJoining,
	}

	salaryBytes, err := json.Marshal(salary)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutPrivateData("collectionSalaryData", strconv.Itoa(employeeID), salaryBytes)
	if err != nil {
		return fmt.Errorf("failed to write to private data collection: %v", err)
	}

	return nil
}

func (ec *EmployeeChaincode) InsertPrivateProjectData(ctx contractapi.TransactionContextInterface, employeeID int, projectDetails string, reportingManager string) error {
	project := Project{
		EmployeeID:       employeeID,
		ProjectDetails:   projectDetails,
		ReportingManager: reportingManager,
	}

	projectBytes, err := json.Marshal(project)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutPrivateData("collectionProjectData", strconv.Itoa(employeeID), projectBytes)
	if err != nil {
		return fmt.Errorf("failed to write to private data collection: %v", err)
	}

	return nil
}

func (ec *EmployeeChaincode) ListAllEmployees(ctx contractapi.TransactionContextInterface) ([]*Employee, error) {
	iterator, err := ctx.GetStub().GetStateByRange("", "")
	if err != nil {
		return nil, fmt.Errorf("failed to get iterator: %v", err)
	}
	defer iterator.Close()

	var employees []*Employee
	for iterator.HasNext() {
		queryResponse, err := iterator.Next()
		if err != nil {
			return nil, fmt.Errorf("failed to get next state: %v", err)
		}

		var employee Employee
		err = json.Unmarshal(queryResponse.Value, &employee)
		if err != nil {
			return nil, err
		}

		employees = append(employees, &employee)
	}

	return employees, nil
}

func (ec *EmployeeChaincode) GetPrivateSalaryData(ctx contractapi.TransactionContextInterface, employeeID int) (*Salary, error) {
	salaryBytes, err := ctx.GetStub().GetPrivateData("collectionSalaryData", strconv.Itoa(employeeID))
	if err != nil {
		return nil, fmt.Errorf("failed to read from private data collection: %v", err)
	}
	if salaryBytes == nil {
		return nil, fmt.Errorf("private salary data for employee with ID %d does not exist", employeeID)
	}

	var salary Salary
	err = json.Unmarshal(salaryBytes, &salary)
	if err != nil {
		return nil, err
	}

	return &salary, nil
}

func (ec *EmployeeChaincode) GetPrivateProjectData(ctx contractapi.TransactionContextInterface, employeeID int) (*Project, error) {
	projectBytes, err := ctx.GetStub().GetPrivateData("collectionProjectData", strconv.Itoa(employeeID))
	if err != nil {
		return nil, fmt.Errorf("failed to read from private data collection: %v", err)
	}
	if projectBytes == nil {
		return nil, fmt.Errorf("private project data for employee with ID %d does not exist", employeeID)
	}

	var project Project
	err = json.Unmarshal(projectBytes, &project)
	if err != nil {
		return nil, err
	}

	return &project, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(&EmployeeChaincode{})
	if err != nil {
		fmt.Printf("Error creating employee chaincode: %v\n", err)
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting employee chaincode: %v\n", err)
	}
}
