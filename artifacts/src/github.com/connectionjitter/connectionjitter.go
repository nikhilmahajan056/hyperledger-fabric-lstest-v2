package main

import (
	"fmt"
	"strconv"
	"encoding/json"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type Logs struct {
	LogID int `json:"logId"`
}

type NspKpiRealTimeKpiEvent struct {
	SessionName       		string `json:"session-name"`
	MetricID          		string `json:"metric-id"`
	Direction         		string `json:"direction"`
	CloseTime     	  		int    `json:"close-time"`
	Suspect           		bool   `json:"suspect"`
	SampleCount       		int    `json:"sample-count"`
	Delay 			  		int    `json:"delay"`
	SystemID      	  		string `json:"system-id"`
	TimeCaptured      		int    `json:"time-captured"`
	TimeCapturedPeriodic	int    `json:"time-captured-periodic"`
	NeID           			string `json:"neId"`
	KPIType 				string `json:"kpiType"`
	ObjectID      			string `json:"objectId"`
	DataType       			int    `json:"dataType"`
}

type IetfRestconfNotification struct {
	EventTime	  	  string 				 `json:"eventTime"`
	RealTimeKpiEvent  NspKpiRealTimeKpiEvent `json:"nsp-kpi:real_time_kpi-event"`
}

type IetfRestConf struct {
	IetfRC    IetfRestconfNotification    `json:"ietf-restconf:notification"`
}

type ConnectionJitter struct {
	Data    IetfRestConf    `json:"data"`
}

type ConnectionJitterChaincode struct {
	contractapi.Contract
}

func (cjc *ConnectionJitterChaincode) InitLedger(ctx contractapi.TransactionContextInterface) error {
	logs := Logs{
		LogID: 0,
	}

	logsJSON, err := json.Marshal(logs)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState("latestLogId", logsJSON)
	if err != nil {
		return fmt.Errorf("failed to write to world state: %v", err)
	}
	return nil
}

func (cjc *ConnectionJitterChaincode) InsertConnectionJitter(ctx contractapi.TransactionContextInterface, sessionName string, metricID string, direction string, closeTime int, suspect bool, sampleCount int, delay int, systemID string, timeCaptured int, timeCapturedPeriodic int, neId string, kpiType string, objectId string, dataType int, eventTime string) error {
	nspKpiRealTimeKpiEvent := NspKpiRealTimeKpiEvent{
		SessionName:			sessionName,
		MetricID:				metricID,
		Direction:				direction,
		CloseTime:				closeTime,
		Suspect:				suspect,
		SampleCount:			sampleCount,
		Delay:					delay,
		SystemID:				systemID,
		TimeCaptured:			timeCaptured,
		TimeCapturedPeriodic:	timeCapturedPeriodic,
		NeID:					neId,
		KPIType:				kpiType,
		ObjectID:				objectId,
		DataType:				dataType,
	}

	ietfRestconfNotification := IetfRestconfNotification{
		EventTime:			eventTime,
		RealTimeKpiEvent:	nspKpiRealTimeKpiEvent,
	}

	ietfRestConf := IetfRestConf{
		IetfRC:		ietfRestconfNotification,
	}

	connectionJitter := ConnectionJitter{
		Data:		ietfRestConf,
	}

	connectionJitterJSON, err := json.Marshal(connectionJitter)
	if err != nil {
		return err
	}

	logId, err := cjc.UpdateLogId(ctx)
	if err != nil {
		return err
	}
	if logId == 0 {
		return fmt.Errorf("Latest LogId can't be found.")
	}

	err = ctx.GetStub().PutState(strconv.Itoa(logId), connectionJitterJSON)
	if err != nil {
		return fmt.Errorf("failed to write to world state: %v", err)
	}

	return nil
}

func (cjc *ConnectionJitterChaincode) UpdateLogId(ctx contractapi.TransactionContextInterface) (int, error) {

	// Check whether latest LogId Already Exists or Not.
	logs, err := cjc.FetchLogID(ctx)
	if err != nil {
		return 0, err
	}
	if logs == nil {
		return 0, fmt.Errorf("The latest logId can't be found. Please try again.")
	}

	logs.LogID += 1;

	logIdJSON, err := json.Marshal(logs)
	if err != nil {
		return 0, err
	}

	err = ctx.GetStub().PutState("latestLogId", logIdJSON)
	if err != nil {
		return 0, fmt.Errorf("failed to write to world state: %v", err)
	}

	return logs.LogID, nil
}

func (cjc *ConnectionJitterChaincode) FetchLogID(ctx contractapi.TransactionContextInterface) (*Logs, error) {
	logBytes, err := ctx.GetStub().GetState("latestLogId")
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if logBytes == nil {
		return nil, fmt.Errorf("latest Log ID does not exist")
	}

	var logs Logs
	err = json.Unmarshal(logBytes, &logs)
	if err != nil {
		return nil, err
	}

	return &logs, nil
}

func (cjc *ConnectionJitterChaincode) FetchLog(ctx contractapi.TransactionContextInterface, logId int) (*ConnectionJitter, error) {
	logBytes, err := ctx.GetStub().GetState(strconv.Itoa(logId))
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if logBytes == nil {
		return nil, fmt.Errorf("log with ID %d does not exist", logId)
	}

	var connectionJitter ConnectionJitter
	err = json.Unmarshal(logBytes, &connectionJitter)
	if err != nil {
		return nil, err
	}

	return &connectionJitter, nil
}

func (cjc *ConnectionJitterChaincode) ListAllLogs(ctx contractapi.TransactionContextInterface) ([]*ConnectionJitter, error) {
	iterator, err := ctx.GetStub().GetStateByRange("", "")
	if err != nil {
		return nil, fmt.Errorf("failed to get iterator: %v", err)
	}
	defer iterator.Close()

	var connectionJitters []*ConnectionJitter
	for iterator.HasNext() {
		queryResponse, err := iterator.Next()
		if err != nil {
			return nil, fmt.Errorf("failed to get next state: %v", err)
		}

		var connectionJitter ConnectionJitter
		err = json.Unmarshal(queryResponse.Value, &connectionJitter)
		if err != nil {
			return nil, err
		}

		connectionJitters = append(connectionJitters, &connectionJitter)
	}

	return connectionJitters, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(&ConnectionJitterChaincode{})
	if err != nil {
		fmt.Printf("Error creating connection jitter chaincode: %v\n", err)
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting connection jitter chaincode: %v\n", err)
	}
}
