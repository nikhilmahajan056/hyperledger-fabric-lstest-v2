const { Gateway, Wallets } = require('fabric-network');
const path = require('path');
import { serverRuntimeConfig } from '../../../next.config.cjs'; 
import { cookies } from 'next/headers'

const identityLabel = 'admin';
const gateway = new Gateway();

export async function GET(request) {

  const cookieStore = cookies()
  const token = cookieStore.get('sessionId');
  
  if (!token) {
    return Response.json({ error: 'Resource not accessible' }, { status: 401 });
  }

  const { searchParams } = new URL(request.url)
  const orgName = searchParams.get('orgName')
  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = 'cjchannel';
    const chaincodeName = 'connectionjitter';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);
    let result = await contract.evaluateTransaction('ListAllLogs', []);
    result = JSON.parse(result.toString());
    
    return Response.json({ logs: result }, { status: 200 });
  } catch (error) {
    return Response.status(500).json({ error: error.message }, { status: 500 });
  }
  
}

export async function POST(request) {
  cookies().set({
    name: 'sessionId',
    value: '',
    maxAge: 0
  });
  return Response.json({ message: 'Logout successful' }, {status: 200});
}

const getCCP = async (org) => {
  let ccp = null;
  org == 'Org1' ? ccp = serverRuntimeConfig.connectionOrg1 : null
  org == 'Org2' ? ccp = serverRuntimeConfig.connectionOrg2 : null  
  return ccp
}

const getWalletPath = async (org) => {
  let walletPath = null
  org == 'Org1' ? walletPath = path.join(process.cwd(), 'wallet', 'org1') : null
  org == 'Org2' ? walletPath = path.join(process.cwd(), 'wallet', 'org2') : null
  return walletPath
}

const getChannelName = async (org) => {
  let channelName = null;
  org == 'Org1' ? channelName = 'org1userchannel' : null
  org == 'Org2' ? channelName = 'org2userchannel' : null
  return channelName
}

async function connectToNetwork(orgName, wallet, channelName, chaincodeName) {
  try {
    let ccp = await getCCP(orgName);
    await gateway.connect(ccp, {
      wallet,
      identity: identityLabel,
      discovery: { enabled: true, asLocalhost: true },
    });
    const network = await gateway.getNetwork(channelName);
    const contract = network.getContract(chaincodeName);
    return contract;
  } catch (error) {
    console.error(`Failed to connect to the network: ${error}`);
  }
}