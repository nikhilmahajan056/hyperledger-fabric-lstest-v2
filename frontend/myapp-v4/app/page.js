import Image from 'next/image'
import styles from './page.module.css'

export default function Home() {
  return (
    <div className={styles.homepage}>
      <div className={styles.header}>
        <a className={styles.signupA} href='/'>
          <Image className={styles.logo} src="/vercel.svg" width={100} height={24} alt="Portal Logo" />
        </a>
        {/* <h1>Employee Management Web Application</h1>           */}
        <div className={styles.headerButtons}>
          <a href="/signup" className={styles.button}>Sign Up</a>
          <a href="/login" className={styles.button}>Login</a>
        </div>
      </div>
      <div className={styles.homepageContent}>
        {/* <h1>Welcome to the Employee Management Portal</h1> */}
        <div className={styles.container}>
          <img className={styles.bannerImage} src="/homepageImage.png" alt="Employee Management" />
          <div>
            <h1 className={styles.homepageHeader}>Introducing Our Log Management Portal</h1>
            <p className={styles.homepagePara}>Efficiently manage your application logs with our state-of-the-art log management portal.</p>
            <p className={styles.homepagePara}>Our platform provides a secure and transparent solution for sharing logs with third party organizations, ensuring seamless coordination and accountability.</p>
            <p className={styles.homepagePara}>With advanced privacy measures in place, your log data remain confidential, guaranteeing confidentiality and maintaining trust between organizations.</p>
          </div>
          
        </div>
      </div>
      <footer className={styles.footer}>
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  )
}
