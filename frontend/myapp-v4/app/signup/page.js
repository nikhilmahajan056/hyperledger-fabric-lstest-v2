'use client'

import Image from 'next/image'
import styles from './page.module.css'
import { useState } from 'react';
import md5 from 'md5';

export default function Signup() {
  const [selectedOrg, setSelectedOrg] = useState('');
  const [fullname, setFullname] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSignup = async () => {
    try {
      console.log("invoked handleSignup", selectedOrg, fullname, username, password, md5(password));
      const response = await fetch('/signup/api', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ fullname, username, password: md5(password), orgName: selectedOrg }),
      });
      const data = await response.json();
      if (response.status === 200) {
        console.log(data);
        alert(data.message);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className={styles.homepage}>
      <div className={styles.header}>
        <a className={styles.signupA} href='/'>
          <Image className={styles.logo} src="/vercel.svg" width={100} height={24} alt="Portal Logo" />
        </a>
        {/* <div className="header-buttons">
          <Link to="/signup" className="button">Sign Up</Link>
          <Link to="/login" className="button">Login</Link>
        </div> */}
      </div>
      <div className={styles.content}>
        <h1>Create a new account</h1>
        <div className={styles.formDiv}>
          <input
            type="text"
            name="fullname"
            placeholder="Full Name"
            className={styles.input}
            onChange={(e) => setFullname(e.target.value)}
            required
          />
          <input
            type="text"
            name="email"
            placeholder="Email"
            className={styles.input}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            className={styles.input}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <select
            value={selectedOrg}
            onChange={(e) => setSelectedOrg(e.target.value)}
            className={styles.signupSelect}
          >
            <option value="">Select Organization</option>
            <option value="Org1">Organization 1</option>
            <option value="Org2">Organization 2</option>
          </select>
          <button className={styles.signupButton} type="submit" onClick={handleSignup}>
            Signup
          </button>
          <p className={styles.p}>Already have an account? <a className={styles.signupA} href="/login">Login</a></p>
        </div>
      </div>
      <footer className={styles.footer}>
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
}
