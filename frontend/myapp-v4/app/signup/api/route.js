import { NextResponse } from 'next/server';
const { Gateway, Wallets } = require('fabric-network');
const FabricCAServices = require('fabric-ca-client');
const path = require('path');
const fs = require('fs');
// const {getRegisteredUser, isUserRegistered} = require('./files/helper')
import { getRegisteredUser, isUserRegistered } from './files/helper';
import { serverRuntimeConfig } from '../../../next.config.cjs'; 


const identityLabel = 'admin';
const gateway = new Gateway();

export async function POST(request) {

  const body = await request.json();
  const { username, password, orgName } = body;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = await getChannelName(orgName);
    const chaincodeName = 'users';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    await contract.submitTransaction('CreateUser', username, password);
    return Response.json({ message: 'User registered successfully' }, {status: 200});
  } catch (error) {
    return Response.json({ error: error.message }, {status: 500});
  }
}

const getCCP = async (org) => {
  let ccp = null;
  org == 'Org1' ? ccp = serverRuntimeConfig.connectionOrg1 : null
  org == 'Org2' ? ccp = serverRuntimeConfig.connectionOrg2 : null  
  return ccp
}

const getWalletPath = async (org) => {
  let walletPath = null
  org == 'Org1' ? walletPath = path.join(process.cwd(), 'wallet', 'org1') : null
  org == 'Org2' ? walletPath = path.join(process.cwd(), 'wallet', 'org2') : null
  return walletPath
}

const getChannelName = async (org) => {
  let channelName = null;
  org == 'Org1' ? channelName = 'org1userchannel' : null
  org == 'Org2' ? channelName = 'org2userchannel' : null
  return channelName
}

async function connectToNetwork(orgName, wallet, channelName, chaincodeName) {
  try {
    let ccp = await getCCP(orgName);
    await gateway.connect(ccp, {
      wallet,
      identity: identityLabel,
      discovery: { enabled: true, asLocalhost: true },
    });
    const network = await gateway.getNetwork(channelName);
    const contract = network.getContract(chaincodeName);
    return contract;
  } catch (error) {
    console.error(`Failed to connect to the network: ${error}`);
  }
}