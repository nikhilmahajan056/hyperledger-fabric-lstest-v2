"use client"; 

import React, {useState} from 'react';
import styles from '../Home.module.css';

const SignupPage = () => {
  const [selectedOrg, setSelectedOrg] = useState('');
  const [fullname, setFullname] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSignup = async () => {
    try {
      console.log("invoked handleSignup", fullname, username, password);
      const response = await fetch('/api/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ fullname, username, password, selectedOrg }),
      });
      const data = await response.json();
      console.log(data.name);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <h1 className={styles.h1}>Welcome to the Employee Management Portal</h1>
        <div className={styles.formDiv}>
          <input
            type="text"
            name="fullname"
            placeholder="Full Name"
            className={styles.input}
            onChange={(e) => setFullname(e.target.value)}
            required
          />
          <input
            type="text"
            name="email"
            placeholder="Email"
            className={styles.input}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            className={styles.input}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <select
            value={selectedOrg}
            onChange={(e) => setSelectedOrg(e.target.value)}
            className={styles.dropdown}
          >
            <option value="">Select Organization</option>
            <option value="Org1">Organization 1</option>
            <option value="Org2">Organization 2</option>
          </select>
          <button type="submit" className={styles.button} onClick={handleSignup}>
            Signup
          </button>
          <p className={styles.p}>Already have an account? <a className={styles.a} href="/">Login</a></p>
        </div>
      </div>
    </div>
  );

  // return (
  //   <div className={styles.container}>
  //     <div className={styles.content}>
  //       <h1 className={styles.h1}>Welcome to the Employee Management Portal</h1>
  //       <div className={styles.formContainer}>
  //         <form className={styles.form} onSubmit={handleLogin}>
  //           <h2 className={styles.h2}>Login</h2>
  //           <input type="email" placeholder="Email" className={styles.input} onChange={(e) => setUsername(e.target.value)}/>
  //           <input type="password" placeholder="Password" className={styles.input} onChange={(e) => setPassword(e.target.value)} />
  //           <button type="submit" className={styles.button}>Login</button>
  //         </form>
  //         <div className={styles.signupContainer}>
  //           <h2 className={styles.h2}>Don't have an account? Sign up!</h2>
  //           <input type="text" placeholder="Full Name" className={styles.input} />
  //           <input type="email" placeholder="Email" className={styles.input} />
  //           <input type="password" placeholder="Password" className={styles.input} />
  //           <button type="submit" className={styles.button}>Sign up</button>
  //         </div>
  //       </div>
  //     </div>
  //   </div>
  // );
};

export default SignupPage;
