import React, {useState} from 'react';
import logo from '../logo.svg';
import '../styles/Login.css';

// Login component
const Login = () => {
  const [selectedOrg, setSelectedOrg] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    try {
      console.log("invoked handleLogin", selectedOrg, username, password);
      const response = await fetch('http://localhost:4000/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password, orgName: selectedOrg }),
      });
      const data = await response.json();
      console.log(data.message);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="homepage">
      <div className="header">
        <a className="login-a" href='/'>
          <img className="logo" src={logo} alt="Portal Logo" />
        </a>
        {/* <div className="header-buttons">
          <Link to="/signup" className="button">Sign Up</Link>
          <Link to="/login" className="button">Login</Link>
        </div> */}
      </div>
      <div className="content">
        <h1>Sign in</h1>
        <div className="formDiv">
          <input
            type="text"
            name="email"
            placeholder="Email"
            // className={styles.input}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            // className={styles.input}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <select
            value={selectedOrg}
            onChange={(e) => setSelectedOrg(e.target.value)}
            className="loginSelect"
          >
            <option value="">Select Organization</option>
            <option value="Org1">Organization 1</option>
            <option value="Org2">Organization 2</option>
          </select>
          <button className="loginButton" type="submit" onClick={handleLogin}>
            Login
          </button>
          <p>Don't have an account? <a href="/signup" className="login-a">Signup</a></p>
        </div>
      </div>
      <footer className="footer">
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
};

export default Login;