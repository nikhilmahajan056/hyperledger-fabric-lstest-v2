import React, {useState} from 'react';
import logo from '../logo.svg';
import '../styles/Signup.css';

// Signup component
const Signup = () => {

  const [selectedOrg, setSelectedOrg] = useState('');
  const [fullname, setFullname] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSignup = async () => {
    try {
      console.log("invoked handleSignup", selectedOrg, fullname, username, password);
      const response = await fetch('http://localhost:4000/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ fullname, username, password, orgName: selectedOrg }),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="homepage">
      <div className="header">
        <a className="signup-a" href='/'>
          <img className="logo" src={logo} alt="Portal Logo" />
        </a>
        {/* <div className="header-buttons">
          <Link to="/signup" className="button">Sign Up</Link>
          <Link to="/login" className="button">Login</Link>
        </div> */}
      </div>
      <div className="content">
        <h1>Create a new account</h1>
        <div className="formDiv">
          <input
            type="text"
            name="fullname"
            placeholder="Full Name"
            // className={styles.input}
            onChange={(e) => setFullname(e.target.value)}
            required
          />
          <input
            type="text"
            name="email"
            placeholder="Email"
            // className={styles.input}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            // className={styles.input}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <select
            value={selectedOrg}
            onChange={(e) => setSelectedOrg(e.target.value)}
            className="signupSelect"
          >
            <option value="">Select Organization</option>
            <option value="Org1">Organization 1</option>
            <option value="Org2">Organization 2</option>
          </select>
          <button className="signupButton" type="submit" onClick={handleSignup}>
            Signup
          </button>
          <p>Already have an account? <a className="signup-a" href="/login">Login</a></p>
        </div>
      </div>
      <footer className="footer">
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
};

export default Signup;