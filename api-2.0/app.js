'use strict';

const bodyParser = require('body-parser');
const http = require('http')
const express = require('express')
const app = express();
const expressJWT = require('express-jwt');
const jwt = require('jsonwebtoken');
const bearerToken = require('express-bearer-token');
const cors = require('cors');
const constants = require('./config/constants.json')

const host = process.env.HOST || constants.host;
const port = process.env.PORT || constants.port;


const {getRegisteredUser, isUserRegistered} = require('./app/helper')
const {invokeTransaction, connection} = require('./app/invoke')
const {query, connect} = require('./app/query')

app.options('*', cors());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
// set secret variable
app.set('secret', constants.appSecret);
app.use(expressJWT({
    secret: constants.appSecret
}).unless({
    path: ['/users','/users/login', '/register','createHeapDump']
}));
app.use(bearerToken());

let connectionFlag_Invoke="N";
let connectionFlag_Query="N";

app.use((req, res, next) => {
    if (req.originalUrl.indexOf('/users') >= 0 || req.originalUrl.indexOf('/users/login') >= 0 || req.originalUrl.indexOf('/register') >= 0) {
        return next();
    }
    let token = req.token;
    jwt.verify(token, app.get('secret'), (err, decoded) => {
        if (err) {
            console.log(`Error ================:${err}`)
            res.send({
                success: false,
                message: 'Failed to authenticate token. Make sure to include the ' +
                    'token returned from /users call in the authorization header ' +
                    ' as a Bearer token'
            });
            return;
        } else {
            req.username = decoded.username;
            req.orgname = decoded.orgName;
            return next();
        }
    });
});

// this is to clean the cache memory
function computeTerm(term) {
    setTimeout(() => {
        delete computeTerm[term];
    }, 1000);
    return computeTerm[term] || (computeTerm[term] = compute());
 
    function compute() {
        return Buffer.alloc(1e3);
    }
}
//end of clean cache memory

let server = http.createServer(app).listen(port, function () { console.log(`Server started on ${port}`) });

server.timeout = 240000;

function getErrorMessage(field) {
    let response = {
        success: false,
        message: field + ' field is missing or Invalid in the request'
    };
    return response;
}

// app.get('/createHeapDump', (req, res) => {
//     try {
//         heapdump.writeSnapshot('/' + Date.now() + '.heapsnapshot');

//         heapdump.writeSnapshot(function (err, filename) {
//             console.log('dump written to', filename);
//         });
//         res.send(`<h2>Snapshot Captured Successfully !!</h2>`);
//     }
//     catch (e) {
//         res.send(`<h2>Snapshot Captured Failed !!</h2>`);
//     }
// })


// Register and enroll user
app.post('/users', async function (req, res) {
    let username = req.body.username;
    let orgName = req.body.orgName;

    if (!username) {
        res.json(getErrorMessage('\'username\''));
        return;
    }
    if (!orgName) {
        res.json(getErrorMessage('\'orgName\''));
        return;
    }

    let token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + parseInt(constants.jwt_expiretime),
        username: username,
        orgName: orgName
    }, app.get('secret'));

    let response = await getRegisteredUser(username, orgName, true);

    if (response && typeof response !== 'string') {

        response.token = token;
        res.json(response);
    } else {
        res.json({ success: false, message: response });
    }

});

// Register and enroll user
// app.post('/register', async function (req, res) {
//     let username = req.body.username;
//     let orgName = req.body.orgName;

//     if (!username) {
//         res.json(getErrorMessage('\'username\''));
//         return;
//     }
//     if (!orgName) {
//         res.json(getErrorMessage('\'orgName\''));
//         return;
//     }

//     let token = jwt.sign({
//         exp: Math.floor(Date.now() / 1000) + parseInt(constants.jwt_expiretime),
//         username: username,
//         orgName: orgName
//     }, app.get('secret'));

//     console.log(token)

//     let response = await helper.registerAndGerSecret(username, orgName);

//     if (response && typeof response !== 'string') {
//         response.token = token;
//         res.json(response);
//     } else {
//         res.json({ success: false, message: response });
//     }

// });

// Login and get jwt
app.post('/users/login', async function (req, res) {
    let username = req.body.username;
    let orgName = req.body.orgName;

    if (!username) {
        res.json(getErrorMessage('\'username\''));
        return;
    }
    if (!orgName) {
        res.json(getErrorMessage('\'orgName\''));
        return;
    }

    let token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + parseInt(constants.jwt_expiretime),
        username: username,
        orgName: orgName
    }, app.get('secret'));

    let isUserRegistered = await isUserRegistered(username, orgName);

    if (isUserRegistered) {
        res.json({ success: true, message: { token: token } });

    } else {
        res.json({ success: false, message: `User with username ${username} is not registered with ${orgName}, Please register first.` });
    }
});

// Invoke transaction on chaincode on target peers
app.post('/channels/:channelName/chaincodes/:chaincodeName', async function (req, res) {

    try {
        let chaincodeName = req.params.chaincodeName;
        let channelName = req.params.channelName;
        let fcn = req.body.fcn;
        let args = req.body.args;

        if (!chaincodeName) {
            res.json(getErrorMessage('\'chaincodeName\''));
            return;
        }
        if (!channelName) {
            res.json(getErrorMessage('\'channelName\''));
            return;
        }
        if (!fcn) {
            res.json(getErrorMessage('\'fcn\''));
            return;
        }
        if (!args) {
            res.json(getErrorMessage('\'args\''));
            return;
        }
        if(connectionFlag_Invoke == "N"){
            await connection(req.username, req.orgname);
            connectionFlag_Invoke="Y";
        }
        let response_payload = await invokeTransaction(channelName, chaincodeName, fcn, args, req.username, req.orgname);
        computeTerm(Math.random());
        res.send(response_payload);

    } catch (error) {
        console.log("app.js: POST Catch Block: ")
        connectionFlag_Invoke="N"
        const response_payload = {
            result: null,
            error: true,
            errorData: error
        }
        console.log(`app.js: POST Catch Block Response Payload: ${response_payload}`)
        res.send(response_payload)
    }
});


app.get('/channels/:channelName/chaincodes/:chaincodeName', async function (req, res) {
    try {

        let channelName = req.params.channelName;
        let chaincodeName = req.params.chaincodeName;
        let args = req.query.args;
        let fcn = req.query.fcn;

        if (!chaincodeName) {
            res.json(getErrorMessage('\'chaincodeName\''));
            return;
        }
        if (!channelName) {
            res.json(getErrorMessage('\'channelName\''));
            return;
        }
        if (!fcn) {
            res.json(getErrorMessage('\'fcn\''));
            return;
        }
        if (!args) {
            res.json(getErrorMessage('\'args\''));
            return;
        }
        console.log('args==========', args);
        args = args.replace(/'/g, '"');
        args = JSON.parse(args);

        if(connectionFlag_Query == "N"){
            await connect(req.username, req.orgname);
            connectionFlag_Query="Y";
        }
        let response_payload = await query(channelName, chaincodeName, args, fcn, req.username, req.orgname);
        computeTerm(Math.random());
        res.send(response_payload);

    } catch (error) {
        console.log("app.js: GET Catch Block: ")
        connectionFlag_Query="N"
        const response_payload = {
            result: null,
            error: true,
            errorData: error
        }
        console.log(`app.js: GET Catch Block Response Payload: ${response_payload}`)
        res.send(response_payload)
    }
});