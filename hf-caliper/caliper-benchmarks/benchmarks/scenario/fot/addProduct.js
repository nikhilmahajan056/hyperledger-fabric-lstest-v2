'use strict';

const { WorkloadModuleBase } = require('@hyperledger/caliper-core');

/**
 * Workload module for the benchmark round.
 */
class AddProductWorkload extends WorkloadModuleBase {
    /**
     * Initializes the workload module instance.
     */
    constructor() {
        super();
        this.txIndex = 0;
    }

    /**
     * Assemble TXs for the round.
     * @return {Promise<TxStatus[]>}
     */
    async submitTransaction() {
        this.txIndex++;

        let uniqueID = 2+this.txIndex;

        let args = {
            contractId: 'fot',
            contractVersion: 'v1',
            contractFunction: 'InsertProductRecords',
            contractArguments: [`{\"UniqueID\":\"${uniqueID}\",\"OrderID\":\"77\",\"ProductID\":\"777\",\"SKUID\":\"7234\",\"ProductName\":\"Chikoo\",\"SellerName\":\"Seller7\",\"CustomerName\":\"Customer7\",\"ScanType\":\"OPS\",\"DateTime\":\"167773999600\",\"Location\":\"loc\",\"MetaData\":\"meta\",\"ReferenceOrder\":\"77\",\"ProductReference\":\"777\"}`],
            timeout: 30
        };

        await this.sutAdapter.sendRequests(args);
    }
}

/**
 * Create a new instance of the workload module.
 * @return {WorkloadModuleInterface}
 */
function createWorkloadModule() {
    return new AddProductWorkload();
}

module.exports.createWorkloadModule = createWorkloadModule;