// backend.js
const express = require('express');
const app = express();
const { Gateway, Wallets } = require('fabric-network');
const path = require('path');
const fs = require('fs');
const cors = require('cors');

const {getRegisteredUser, isUserRegistered} = require('./app/helper')

app.options('*', cors());
app.use(cors());

// const ccpPath = path.resolve(__dirname, 'connection.json');
// const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
// const ccp = JSON.parse(ccpJSON);

// const walletPath = path.join(process.cwd(), 'wallet');
// var wallet; // = await Wallets.newFileSystemWallet(walletPath);
const identityLabel = 'admin';
const gateway = new Gateway();

const getCCP = async (org) => {
  let ccpPath = null;
  org == 'Org1' ? ccpPath = path.resolve(__dirname, 'config', 'connection-org1.json') : null
  org == 'Org2' ? ccpPath = path.resolve(__dirname, 'config', 'connection-org2.json') : null
  const ccpJSON = fs.readFileSync(ccpPath, 'utf8')
  const ccp = JSON.parse(ccpJSON);
  return ccp
}

const getWalletPath = async (org) => {
  let walletPath = null
  org == 'Org1' ? walletPath = path.join(process.cwd(), 'wallet', 'org1') : null
  org == 'Org2' ? walletPath = path.join(process.cwd(), 'wallet', 'org2') : null
  return walletPath
}

const getChannelName = async (org) => {
  let channelName = null;
  org == 'Org1' ? channelName = 'org1userchannel' : null
  org == 'Org2' ? channelName = 'org2userchannel' : null
  return channelName
}

async function connectToNetwork(orgName, wallet, channelName, chaincodeName) {
  try {
    let ccp = await getCCP(orgName);
    await gateway.connect(ccp, {
      wallet,
      identity: identityLabel,
      discovery: { enabled: true, asLocalhost: true },
    });
    const network = await gateway.getNetwork(channelName);
    const contract = network.getContract(chaincodeName);
    return contract;
  } catch (error) {
    console.error(`Failed to connect to the network: ${error}`);
  }
}

app.use(express.json());


function getErrorMessage(field) {
    let response = {
        success: false,
        message: field + ' field is missing or Invalid in the request'
    };
    return response;
}

app.post('/users', async function (req, res) {
    let username = req.body.username;
    let orgName = req.body.orgName;

    if (!username) {
        res.json(getErrorMessage('\'username\''));
        return;
    }
    if (!orgName) {
        res.json(getErrorMessage('\'orgName\''));
        return;
    }

    let response = await getRegisteredUser(username, orgName, true);

    if (response && typeof response !== 'string') {
        res.json(response);
    } else {
        res.json({ success: false, message: response });
    }

});

app.post('/signup', async (req, res) => {
  const { username, password, orgName } = req.body;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = await getChannelName(orgName);
    const chaincodeName = 'users';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    await contract.submitTransaction('CreateUser', username, password);
    res.status(200).json({ message: 'User registered successfully' });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.post('/login', async (req, res) => {
  const { username, password, orgName } = req.body;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = await getChannelName(orgName);
    const chaincodeName = 'users';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    const result = await contract.evaluateTransaction('AuthenticateUser', username, password);
    if (result.toString() === 'true') {
      res.status(200).json({ message: 'Login successful' });
    } else {
      res.status(401).json({ error: 'Invalid username or password' });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.post('/createLog', async (req, res) => {
  const { orgName, sessionName, metricID, direction, closeTime, suspect, sampleCount, delay, systemID, timeCaptured, timeCapturedPeriodic, neId, kpiType, objectId, dataType, eventTime } = req.body;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = 'cjchannel';
    const chaincodeName = 'connectionjitter';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    await contract.submitTransaction('InsertConnectionJitter', sessionName, metricID, direction, closeTime, suspect, sampleCount, delay, systemID, timeCaptured, timeCapturedPeriodic, neId, kpiType, objectId, dataType, eventTime);
    res.status(200).json({ message: 'Log registered successfully' });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.get('/getAllLogs/:orgName', async (req, res) => {
  const orgName = req.params.orgName;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = 'cjchannel';
    const chaincodeName = 'connectionjitter';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    let result = await contract.evaluateTransaction('ListAllLogs', []);
    result = JSON.parse(result.toString());
    res.status(200).json({ logs: result });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.get('/getLog/:orgName/:logId', async (req, res) => {
  const orgName = req.params.orgName;
  const logId = req.params.logId;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = 'cjchannel';
    const chaincodeName = 'connectionjitter';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    let result = await contract.evaluateTransaction('FetchLog', [logId]);
    result = JSON.parse(result.toString());
    res.status(200).json({ logs: result });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.put('/updateEmployee', async (req, res) => {
  const { orgName, employeeID, firstName, lastName, contactNumber, emailID, address, yearsOfExperience, technologies, isAvailable } = req.body;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = 'employeeschannel';
    const chaincodeName = 'employees';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    await contract.submitTransaction('UpdateEmployee', employeeID.toString(), firstName, lastName, contactNumber.toString(), emailID, address, yearsOfExperience.toString(), technologies, isAvailable.toString());
    res.status(200).json({ message: 'Employee updated successfully' });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.post('/moveEmployee', async (req, res) => {
  const { orgName, employeeID, firstName, lastName, contactNumber, emailID, address, yearsOfExperience, technologies, isAvailable, projectDetails, reportingManager, salaryDetails, dateOfJoining } = req.body;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = 'employeeschannel';
    const chaincodeName = 'employees';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    await contract.submitTransaction('UpdateEmployee', employeeID.toString(), firstName, lastName, contactNumber.toString(), emailID, address, yearsOfExperience.toString(), technologies, isAvailable.toString());
    await contract.submitTransaction('InsertPrivateSalaryData', employeeID.toString(), salaryDetails, dateOfJoining.toString());
    await contract.submitTransaction('InsertPrivateProjectData', employeeID.toString(), projectDetails, reportingManager);
    res.status(200).json({ message: 'Employee moved successfully' });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.get('/getPrivateEmployeeData/:orgName/:employeeID', async (req, res) => {
  const orgName = req.params.orgName;
  const employeeID = req.params.employeeID;

  try {
    const walletPath = await getWalletPath(orgName)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const channelName = 'employeeschannel';
    const chaincodeName = 'employees';
    const contract = await connectToNetwork(orgName, wallet, channelName, chaincodeName);

    let salaryData = await contract.evaluateTransaction('GetPrivateSalaryData', employeeID.toString());
    salaryData = JSON.parse(salaryData.toString());
    let projectData = await contract.evaluateTransaction('GetPrivateProjectData', employeeID.toString());
    projectData = JSON.parse(projectData.toString());
    let result = {...salaryData, ...projectData};
    res.status(200).json({ result });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.listen(4000, () => {
  console.log('Server is running on http://localhost:4000');
});
